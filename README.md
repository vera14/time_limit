This is a code snippet intended for use with a Matlab based behavioral experiment in Psychtoolbox. It is a loop that will check minutes after the hour on each iteration, and then end the loop when that limit has been exceeded.

This can be used to prevent experiments from running over the time limit, and to end the experiment nicely so no data is lost.

Talk to Vera if you have any questions or want to add features.