% This code is a loop that can be placed around each trial in your main
% experiment code. It will check if the minutes past the hour are equal to
% or greater than the variable minutes_after, and end the experiment if this
% time limit has been exceeded.

% Vera 10/13/16

% Set the number of minutes after the hour when you want the experiment to
% end
minutes_after = 55;

current_minutes = 0;
% Start the loop
while minutes_after > current_minutes
    
    % Put your experiment code here
    
    % Get the time and date from the computer and update current minutes
    time_bits = strsplit(datestr(now), ':');
    current_minutes = str2double(time_bits{2});
end

% Make sure to save your data one last time before closing the experiment!